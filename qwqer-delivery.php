<?php

/**
 * @wordpress-plugin
 * Plugin Name:       QWQER Delivery
 * Plugin URI:        https://qwqer.eu
 * Description:       QWQER Delivery plugin for WooCommerce shop
 * Version:           0.0.8
 * Author:            QWQER
 * Author URI:        https://qwqer.eu
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       qwqer-delivery
 * Domain Path:       /languages
 */

if (!defined('WPINC')) {
	die;
}

define('QWQER_DELIVERY_VERSION', '0.0.8');

function activate_qwqer_delivery()
{
	require_once plugin_dir_path(__FILE__) . 'includes/class-qwqer-delivery-activator.php';
	Qwqer_Delivery_Activator::activate();
}

function deactivate_qwqer_delivery()
{
	require_once plugin_dir_path(__FILE__) . 'includes/class-qwqer-delivery-deactivator.php';
	Qwqer_Delivery_Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_qwqer_delivery');
register_deactivation_hook(__FILE__, 'deactivate_qwqer_delivery');

require plugin_dir_path(__FILE__) . 'includes/class-qwqer-delivery.php';

function run_qwqer_delivery()
{
	$plugin = new Qwqer_Delivery();
	$plugin->run();
}

run_qwqer_delivery();
