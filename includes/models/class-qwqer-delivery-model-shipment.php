<?php

class Qwqer_Delivery_Model_Shipment extends Qwqer_Delivery_Model
{
	protected $tableName = 'shipment';

	/**
	 * @param null $status
	 * @param int $page
	 * @return array
	 */
	public static function getAll($status = null, $page = 1)
	{
		$self = new static();

		if ($status) {
			$rows = $self->wpdb->get_results(
				"SELECT SQL_CALC_FOUND_ROWS * FROM " . $self->getTableName()
				. " WHERE status = " . (int)$status
				. " LIMIT " . (($page - 1) * $self->getPerPage()) . ", " . $self->getPerPage(),
				ARRAY_A
			);
		} else {
			$rows = $self->wpdb->get_results(
				"SELECT SQL_CALC_FOUND_ROWS * FROM " . $self->getTableName()
				. " LIMIT " . (($page - 1) * $self->getPerPage()) . ", " . $self->getPerPage(),
				ARRAY_A
			);
		}

		return $rows;
	}

	/**
	 * @param int $id
	 * @return static|null
	 */
	public static function getByOrderId($id)
	{
		$self = new static();

		$row = $self->wpdb->get_row("SELECT * FROM " . $self->getTableName() . " WHERE id_order = " . (int)$id . " ORDER BY `id` DESC", ARRAY_A);

		if (!$row) {
			return null;
		}

		return new static($row);
	}
}
