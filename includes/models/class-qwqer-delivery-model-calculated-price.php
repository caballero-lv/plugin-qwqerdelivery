<?php

class Qwqer_Delivery_Model_Calculated_Price extends Qwqer_Delivery_Model
{
	protected $tableName = 'calculated_price';

    /**
     * @param string $hash
     * @return static|null
     */
	public static function getByHash($hash)
    {
        $self = new static();

        $row = $self->wpdb->get_row("SELECT * FROM " . $self->getTableName() . " WHERE hash = '" . (string)$hash . "'", ARRAY_A);

        if (!$row) {
            return null;
        }

        return new static($row);
    }
}
