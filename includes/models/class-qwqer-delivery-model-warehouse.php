<?php

class Qwqer_Delivery_Model_Warehouse extends Qwqer_Delivery_Model
{
	protected $tableName = 'warehouse';

	/**
	 * @param int $id
	 */
	public static function removeDefaultExlcludeId($id)
	{
		$self = new static();

		$self->wpdb->query(
			$self->wpdb->prepare(
				"UPDATE " . $self->getTableName() . " SET is_default = 0 WHERE " . $self->getPrimaryKey() . " != " . (int)$id
			)
		);
	}

    /**
     * @return static|null
     */
	public static function getDefaultOrFirst()
    {
        $self = new static();

        $row = $self->wpdb->get_row("SELECT * FROM " . $self->getTableName() . " WHERE is_default = 1", ARRAY_A);

        if ($row) {
            return new static($row);
        }

        $row = $self->wpdb->get_row("SELECT * FROM " . $self->getTableName(), ARRAY_A);

        if (!$row) {
            return null;
        }

        return new static($row);
    }
}
