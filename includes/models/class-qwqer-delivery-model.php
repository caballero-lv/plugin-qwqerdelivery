<?php

abstract class Qwqer_Delivery_Model
{
	/** @var string */
	protected $tableName;

	/** @var string */
	protected $primaryKey = 'id';

	/** @var int */
	protected $perPage = 10;

	protected $wpdb;
	protected $pluginName = 'qwqer_delivery';

	/** @var int */
	public $id;
	/** @var array */
	public $data;

	public function __construct($data = null)
	{
		global $wpdb;

		$this->wpdb = $wpdb;

		if (!empty($data)) {
			$this->id = $data[$this->getPrimaryKey()] ?: null;
			$this->data = array_filter($data, function ($value, $key) {
				return $key !== $this->getPrimaryKey();
			}, ARRAY_FILTER_USE_BOTH);
		}
	}

	/**
	 * @return string
	 */
	public function getTableName()
	{
		return $this->wpdb->prefix . $this->pluginName . '_' . $this->tableName;
	}

	/**
	 * @return string
	 */
	public function getPrimaryKey()
	{
		return $this->primaryKey;
	}

	/**
	 * @return int
	 */
	public function getPerPage()
	{
		return $this->perPage;
	}

	/**
	 * @return $this
	 */
	public function save()
	{
		if ($this->id) {
			$this->wpdb->update($this->getTableName(), $this->data, [
				$this->getPrimaryKey() => $this->id
			]);
		} else {
			$this->wpdb->insert($this->getTableName(), $this->data);
			$this->id =& $this->wpdb->insert_id;
		}

		$model = self::getById($this->id);
		$this->data =& $model->data;

		return $this;
	}

	public function delete()
	{
		if (!$this->id) {
			return;
		}

		$this->wpdb->delete($this->getTableName(), [
			$this->getPrimaryKey() => $this->id
		]);
	}

	/**
	 * @return array
	 */
	public static function all()
	{
		$self = new static();

		return $self->wpdb->get_results(
			"SELECT * FROM " . $self->getTableName(),
			ARRAY_A
		);
	}

	/**
	 * @param int $id
	 * @return static|null
	 */
	public static function getById($id)
	{
		$self = new static();

		$row = $self->wpdb->get_row(
			"SELECT * FROM " . $self->getTableName()
			. " WHERE " . $self->getPrimaryKey() . " = " . (int)$id,
			ARRAY_A
		);

		if (!$row) {
			return null;
		}

		return new static($row);
	}

	/**
	 * @return array
	 */
	public function toArray() {
		return array_merge(['id' => $this->id], $this->data);
	}

	/**
	 * @return int
	 */
	public function getPagesCount()
	{
		$total = $this->wpdb->get_row('SELECT FOUND_ROWS();', ARRAY_A);

		return (int)($total['FOUND_ROWS()'] ?: 1);
	}
}
