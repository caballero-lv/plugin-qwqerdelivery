<?php

class Qwqer_Delivery_Password
{
	const ENCRYPTION_KEY = 'UFeN?^rwsD3Wy-G*';
	const CIPHER = 'AES-128-CBC';

	/**
	 * @param $rawPassword
	 * @return string
	 */
	public function encrypt($rawPassword)
	{
		$ivlen = openssl_cipher_iv_length(self::CIPHER);
		$iv = openssl_random_pseudo_bytes($ivlen);

		$ciphertext_raw = openssl_encrypt($rawPassword, self::CIPHER, self::ENCRYPTION_KEY, OPENSSL_RAW_DATA, $iv);
		$hmac = hash_hmac('sha256', $ciphertext_raw, self::ENCRYPTION_KEY, true);

		return base64_encode($iv . $hmac . $ciphertext_raw);
	}

	/**
	 * @param $encryptedPassword
	 * @return string|null
	 */
	public function decrypt($encryptedPassword)
	{
		$sha2len = 32;
		$c = base64_decode($encryptedPassword);

		$ivlen = openssl_cipher_iv_length(self::CIPHER);
		$iv = substr($c, 0, $ivlen);
		$hmac = substr($c, $ivlen, $sha2len);
		$ciphertext_raw = substr($c, $ivlen + $sha2len);

		$original_plaintext = openssl_decrypt($ciphertext_raw, self::CIPHER, self::ENCRYPTION_KEY, $options=OPENSSL_RAW_DATA, $iv);
		$calcmac = hash_hmac('sha256', $ciphertext_raw, self::ENCRYPTION_KEY, true);

		return hash_equals($hmac, $calcmac) ? $original_plaintext : null;
	}
}
