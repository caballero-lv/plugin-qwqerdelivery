<?php

class Qwqer_Delivery
{
	/** @var Qwqer_Delivery_Loader */
	protected $loader;

	/** @var string */
	protected $qwqer_delivery;

	/** @var string */
	protected $version;

	public function __construct()
	{
		if (defined('QWQER_DELIVERY_VERSION')) {
			$this->version = QWQER_DELIVERY_VERSION;
		} else {
			$this->version = '0.0.2';
		}
		$this->qwqer_delivery = 'qwqer-delivery';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
	}

	private function load_dependencies()
	{
		require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-qwqer-delivery-loader.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-qwqer-delivery-i18n.php';

		require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-qwqer-delivery-password.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-qwqer-delivery-request.php';

		require_once plugin_dir_path(dirname(__FILE__)) . 'includes/models/class-qwqer-delivery-model.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'includes/models/class-qwqer-delivery-model-warehouse.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'includes/models/class-qwqer-delivery-model-shipment.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'includes/models/class-qwqer-delivery-model-calculated-price.php';

		require_once plugin_dir_path(dirname(__FILE__)) . 'admin/controllers/qwqer-delivery-admin-controller.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'admin/controllers/qwqer-delivery-admin-controllers-configs.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'admin/controllers/qwqer-delivery-admin-controllers-warehouses.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'admin/controllers/qwqer-delivery-admin-controllers-shipment.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'admin/controllers/qwqer-delivery-admin-controllers-order.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'admin/controllers/qwqer-delivery-admin-controllers-process.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'admin/controllers/qwqer-delivery-admin-controllers-translations.php';

		require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-qwqer-delivery-admin.php';

		require_once plugin_dir_path(dirname(__FILE__)) . 'public/class-qwqer-delivery-public.php';

		$this->loader = new Qwqer_Delivery_Loader();
	}

	private function set_locale()
	{
		$plugin_i18n = new Qwqer_Delivery_i18n();

		$this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');
	}

	private function define_admin_hooks()
	{
		global $plugin_admin;

		$plugin_admin = new Qwqer_Delivery_Admin($this->get_qwqer_delivery(), $this->get_version());

		$this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_styles');
		$this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts');

		$this->loader->add_action('admin_menu', $plugin_admin, 'admin_menu_action');

		$plugin_admin->define_ajax_actions();

		function qwqer_delivery_register_meta_boxes() {
			add_meta_box('qwqer_delivery', __('QWQER Delivery', 'qwqer-delivery'), 'qwqer_delivery_register_qwqer_delivery_meta_box', 'shop_order', 'normal', 'default');
		}

		add_action('add_meta_boxes', 'qwqer_delivery_register_meta_boxes', 30);

		function qwqer_delivery_register_qwqer_delivery_meta_box($order) {
			global $plugin_admin;

			echo $plugin_admin->enqueue_order_details($order->ID);
		}
	}

	private function define_public_hooks()
	{
		$plugin_public = new Qwqer_Delivery_Public($this->get_qwqer_delivery(), $this->get_version());

		$this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_styles');
		$this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_scripts');

        $this->loader->add_action('woocommerce_shipping_init', $this, 'shipping_method_init');
        $this->loader->add_filter('woocommerce_shipping_methods', $this, 'shipping_method');
    }

    public function shipping_method($methods)
    {
        $methods['qwqer_shipping_method'] = 'Qwqer_Delivery_Shipping_Method';

        return $methods;
    }

    public function shipping_method_init()
    {
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-qwqer-delivery-shipping-method.php';
    }

	public function run()
	{
		$this->loader->run();
	}

    /**
     * @return string
     */
	public function get_qwqer_delivery()
	{
		return $this->qwqer_delivery;
	}

    /**
     * @return Qwqer_Delivery_Loader
     */
	public function get_loader()
	{
		return $this->loader;
	}

    /**
     * @return string
     */
	public function get_version()
	{
		return $this->version;
	}

}
