<?php

class Qwqer_Delivery_Shipping_Method extends WC_Shipping_Method
{
    protected $requestService;
    protected $passwordService;

    protected $warehouse;

    private $useCustomTranslations = false;

    /**
     * Constructor for your shipping class
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        global $locale;

        $this->requestService = new Qwqer_Delivery_Request();
        $this->passwordService = new Qwqer_Delivery_Password();

        $this->id = 'qwqer_shipping_method'; // Id for your shipping method. Should be uunique.
        $this->method_title = __('QWQER Shipping Method', 'qwqer-delivery');  // Title shown in admin
        $this->method_description = __('QWQER Delivery, that doesn\'t take away your time', 'qwqer-delivery'); // Description shown in admin

        $this->countries = ['LV'];

        // $this->init();

        $this->enabled = get_option('qwqer_delivery_enabled') ? 'yes' : 'no';
        $this->title = __('QWQER Delivery within 2 hours', 'qwqer-delivery');

        $translations = get_option('qwqer_delivery_translations', []);
        if (array_key_exists($locale, $translations)) {
            $translation = $translations[ $locale ];

            if ( isset( $translation['enabled'], $translation['translations'] ) && $translation['enabled'] && ! empty( $translation['translations'] ) && ! empty( $translation['translations']['shippingMethod'] ) ) {
                $this->useCustomTranslations = true;
                $this->title = "QWQER: {$translation['translations']['shippingMethod']}";
            }
        }


        $this->warehouse = Qwqer_Delivery_Model_Warehouse::getDefaultOrFirst();

        if (!$this->warehouse) {
            $this->enabled = 'no';
        }
    }

    /**
     * Init your settings
     *
     * @access public
     * @return void
     */
    public function init()
    {
        // Load the settings API
        $this->init_form_fields(); // This is part of the settings API. Override the method to add your own settings
        $this->init_settings(); // This is part of the settings API. Loads settings you previously init.

        // Save settings in admin if you have any defined
        add_action('woocommerce_update_options_shipping_' . $this->id, array($this, 'process_admin_options'));
    }

    public function init_form_fields()
    {
        $this->form_fields = [
            'active' => [
                'title' => 'Shipping method enabled',
                'description' => 'This controls the shipping method is enabled or no.',
                'type' => 'checkbox',
                'default' => 'yes',
                'label' => 'Active',
            ],
        ];
    }

    /**
     * calculate_shipping function.
     *
     * @access public
     * @param array $package
     * @return void
     */
    public function calculate_shipping( $package = array() )
    {
        $destination = $package['destination'];
        $user = (new WC_Customer($package['user']['ID'], true))->get_data();

        if (!$destination['address']) {
            $this->calculatedRate(
                $package,
                $this->useCustomTranslations
                    ? $this->title
                    : $this->title . ' (' . __('starting from 2.50 €', 'qwqer-delivery') . ')'
            );

            return;
        }

        $senderData = array_merge(
            [
                'name' => get_the_title(get_option('woocommerce_shop_page_id')),
                // 'phone' => Configuration::get('PS_SHOP_PHONE') ?: null,
                // 'contact' => Configuration::get('PS_SHOP_PHONE') ?: null,
                // 'email' => Configuration::get('PS_SHOP_EMAIL') ?: null,
                // 'company' => Configuration::get('PS_SHOP_NAME') ?: null
            ],
            $this->warehouse->data,
            [
                'address' => implode(', ', array_filter([
                    $this->warehouse->data['address'],
                    $this->warehouse->data['city'],
                    $this->warehouse->data['country'],
                    $this->warehouse->data['state'],
                    $this->warehouse->data['region'],
                    $this->warehouse->data['zipcode']
                ]))
            ]
        );

        $receiverData = array_merge(
            [
                'name' => implode(' ', array_filter([
                    $user['shipping']['first_name'] ?: $user['billing']['first_name'],
                    $user['shipping']['last_name'] ?: $user['billing']['last_name']
                ])),
                'contact' => $user['billing']['phone'],
                'phone' => $user['billing']['phone'],
                'email' => $user['billing']['email'],
                'company' => $user['shipping']['company'] ?: $user['billing']['company']
            ],
            [
                'country' => 'Latvia',
                'countrycode2' => $destination['country'],
                'city' => $destination['city'],
                'citycode2' => '',
                'zipcode' => $destination['postcode'],
                'state' => $destination['state'],
                'statecode' => '',
                'region' => '',
                'address' => implode(', ', array_filter([
                    $destination['address'],
                    $destination['city'],
                    'Latvia',
                    $destination['state'],
                    $destination['postcode']
                ]))
            ]
        );

        $ordersizeData = [
            'length'     => 0,
            'width'      => 0,
            'height'     => 0,
            // 'weight' => round($parcelsTotalWeight) ?: 1,
            'weight'     => 1,
            'lenunit'    => 'CENTIMETER',
            'weightunit' => 'KILOGRAM',
        ];

        $cacheHash = md5(implode('|', $senderData))
                     . ':' . md5(implode('|', $receiverData))
                     . ':' . md5(implode('|', $ordersizeData));

        $cachedPrice = Qwqer_Delivery_Model_Calculated_Price::getByHash($cacheHash);

        if ($cachedPrice && time() - strtotime($cachedPrice->data['address_delivery_date']) <= 2592000) {
            $payload = json_decode($cachedPrice->data['payload'], true);

            if ($payload['distance'] > 40000) {
                return;
            }

            $this->calculatedRate($package, $this->title, $payload['price']);

            return;
        }

        // Login to QWQER Api
        try {
            $loginResponse = $this->requestService->post('/api/xr/mch/login', [
                'login' => get_option('qwqer_delivery_login'),
                'passw' => $this->passwordService->decrypt(get_option('qwqer_delivery_password'))
            ]);

            $token = $loginResponse['data']['restid'];
        } catch (exception $exception) {
            return;
        }

        // Delivery order price from QWQER Api
        try {
            $deliveryOrderPriceResponse = $this->requestService->post('/api/xr/mch/delivery_price', [
                'sender' => $senderData,
                'receiver' => $receiverData,
                'ordersize' => $ordersizeData,
            ], [
                "Authorization: Bearer {$token}"
            ]);
        } catch (exception $exception) {
            return;
        }

        if (!$cachedPrice) {
            $cachedPrice = new Qwqer_Delivery_Model_Calculated_Price(['hash' => $cacheHash]);
        }

        $cachedPrice->data['price'] = (int)((float)$deliveryOrderPriceResponse['data']['price'] * 100);
        $cachedPrice->data['payload'] = json_encode($deliveryOrderPriceResponse['data']);
        $cachedPrice->data['address_delivery_date'] = date('Y-m-d H:i:s');
        $cachedPrice->save();

        if ($deliveryOrderPriceResponse['data']['distance'] > 40000) {
        	return;
        }

        $this->calculatedRate($package, $this->title, $deliveryOrderPriceResponse['data']['price']);
    }

    protected function calculatedRate($package, $title, $price = null)
    {
        $rate = [
            'id' => $this->get_rate_id(),
            'label' => $title,
            'cost' => $price ? (string)$price : null,
            'calc_tax' => 'per_order',
            'package' => $package,
        ];

        // Register the rate
        $this->add_rate($rate);
    }
}
