<?php

class Qwqer_Delivery_Request
{
    const API_URL = 'https://qwqer.eu';

    protected function getUserAgentString()
    {
        global $wp_version;

        return 'WordPress version: ' . $wp_version . '; WooCommerce version: ' . WC_VERSION . '; Plugin version: ' . QWQER_DELIVERY_VERSION;
    }

    public function post($url, $data = [], $headers = [])
    {
        $data_string = json_encode($data);

        $ch = curl_init(self::API_URL . $url);
        curl_setopt($ch, CURLOPT_USERAGENT, $this->getUserAgentString());
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array_merge([
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string)
        ], $headers));

        return $this->request($ch);
    }

    public function delete($url, $data = [], $headers = [])
    {
        $data_string = json_encode($data);

        $ch = curl_init(self::API_URL . $url);
        curl_setopt($ch, CURLOPT_USERAGENT, $this->getUserAgentString());
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array_merge([
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string)
        ], $headers));

        return $this->request($ch);
    }

    public function get($url, $params = [], $headers = [])
    {
        $ch = curl_init(self::API_URL . $url . http_build_query($params));
        curl_setopt($ch, CURLOPT_USERAGENT, $this->getUserAgentString());
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array_merge([
            'Content-Type: application/json'
        ], $headers));

        return $this->request($ch);
    }

    private function request($ch)
    {
        $rawResponse = curl_exec($ch);

        if (curl_errno($ch)) {
            throw new ErrorException(curl_error($ch));
        }

        curl_close($ch);

        $response = json_decode($rawResponse, true);

        if (json_last_error()) {
            throw new ErrorException(json_last_error_msg());
        }

        if ($response['rstate'] !== 0) {
            throw new ErrorException($response['rmsg']);
        }

        return $response;
    }
}
