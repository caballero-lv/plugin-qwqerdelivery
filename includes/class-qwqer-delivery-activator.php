<?php

class Qwqer_Delivery_Activator
{
	public static function activate()
	{
		global $wpdb;

		$dbPrefix = $wpdb->prefix;
		$pluginName = 'qwqer_delivery';

		$sql = [];

		$sql[] = "CREATE TABLE IF NOT EXISTS `".$dbPrefix.$pluginName."_warehouse` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `is_default` TINYINT(1) DEFAULT 0,
            `country` VARCHAR(255) NOT NULL,
            `countrycode2` VARCHAR(255) NOT NULL,
            `city` VARCHAR(255) NOT NULL,
            `citycode2` VARCHAR(255) NULL,
            `zipcode` VARCHAR(255) NOT NULL,
            `state` VARCHAR(255) NULL,
            `statecode` VARCHAR(255) NULL,
            `region` VARCHAR(255) NULL,
            `address` MEDIUMTEXT NOT NULL,
            PRIMARY KEY  (`id`)
        ) CHARSET=utf8;";

		$sql[] = "CREATE TABLE IF NOT EXISTS `".$dbPrefix.$pluginName."_shipment` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `id_order` INT(11) UNSIGNED NOT NULL,
            `id_qwqer_order` INT(11) UNSIGNED NOT NULL,
            `status` VARCHAR(255) NOT NULL,
            `price` INT(11) UNSIGNED NOT NULL,
            `payload` MEDIUMTEXT NOT NULL,
            PRIMARY KEY  (`id`)
        ) CHARSET=utf8;";

		$sql[] = "CREATE TABLE IF NOT EXISTS `".$dbPrefix.$pluginName."_calculated_price` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `hash` VARCHAR(255) NOT NULL,
            `price` INT(11) UNSIGNED NOT NULL,
            `payload` MEDIUMTEXT NULL,
            `address_delivery_date` DATETIME NOT NULL,
            PRIMARY KEY  (`id`)
        ) CHARSET=utf8;";

		foreach ($sql as $query) {
			$wpdb->query($query);
		}
	}
}
