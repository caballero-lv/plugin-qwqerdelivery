<?php

class Qwqer_Delivery_Deactivator
{
	public static function deactivate()
	{
		global $wpdb;

		$dbPrefix = $wpdb->prefix;
		$pluginName = 'qwqer_delivery';

		$sql = [];

		$sql[] = "DROP TABLE IF EXISTS `".$dbPrefix.$pluginName."_warehouse`";
		$sql[] = "DROP TABLE IF EXISTS `".$dbPrefix.$pluginName."_shipment`";
		$sql[] = "DROP TABLE IF EXISTS `".$dbPrefix.$pluginName."_calculated_price`";

		foreach ($sql as $query) {
			$wpdb->query($query);
		}
	}
}
