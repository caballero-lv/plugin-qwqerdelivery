<?php

class Qwqer_Delivery_i18n
{
	public function load_plugin_textdomain()
	{
		load_plugin_textdomain(
			'qwqer-delivery',
			false,
			dirname(dirname(plugin_basename(__FILE__))) . '/languages/'
		);
	}
}
