<?php

class Qwqer_Delivery_Admin
{
    /** @var string */
	private $qwqer_delivery;

    /** @var string */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @param string $qwqer_delivery The name of this plugin.
	 * @param string $version The version of this plugin.
	 */
	public function __construct($qwqer_delivery, $version)
	{
		$this->qwqer_delivery = $qwqer_delivery;
		$this->version = $version;
	}

	public function define_ajax_actions()
	{
		$configs_controller = new Qwqer_Delivery_Admin_Controllers_Configs();
		$translations_controller = new Qwqer_Delivery_Admin_Controllers_Translations();
		$warehouses_controller = new Qwqer_Delivery_Admin_Controllers_Warehouses();
		$order_controller = new Qwqer_Delivery_Admin_Controllers_Order();
		$shipment_controller = new Qwqer_Delivery_Admin_Controllers_Shipment();
		$process_controller = new Qwqer_Delivery_Admin_Controllers_Process();

		add_action('wp_ajax_qwqer_configs_get', [$configs_controller, 'get']);
		add_action('wp_ajax_qwqer_configs_post', [$configs_controller, 'post']);

		add_action('wp_ajax_qwqer_translations_get', [$translations_controller, 'get']);
		add_action('wp_ajax_qwqer_translations_post', [$translations_controller, 'post']);

		add_action('wp_ajax_qwqer_warehouses_get', [$warehouses_controller, 'get']);
		add_action('wp_ajax_qwqer_warehouses_post', [$warehouses_controller, 'post']);
		add_action('wp_ajax_qwqer_warehouses_delete', [$warehouses_controller, 'delete']);

		add_action('wp_ajax_qwqer_order_get', [$order_controller, 'get']);

		add_action('wp_ajax_qwqer_process_get_price', [$process_controller, 'get_price']);
		add_action('wp_ajax_qwqer_process_place_order', [$process_controller, 'place_order']);

		add_action('wp_ajax_qwqer_shipment_by_order_id', [$shipment_controller, 'get_by_order_id']);
		add_action('wp_ajax_qwqer_shipment_update_status', [$shipment_controller, 'update_status']);
		add_action('wp_ajax_qwqer_shipment_list', [$shipment_controller, 'get_all']);
		add_action('wp_ajax_qwqer_shipment_reject', [$shipment_controller, 'reject']);
	}

	public function admin_menu_action()
	{
		add_submenu_page(
			'options-general.php',
			$this->qwqer_delivery,
			__('QWQER Settings', 'qwqer-delivery'),
			'administrator',
			"{$this->qwqer_delivery}-settings",
			[$this, 'admin_menu']
		);
	}

	protected function viewJsVariables()
	{
		$qwqerModule = [
			'link' => [
				'getConfigs' => 'qwqer_configs_get',
				'updateConfigs' => 'qwqer_configs_post',

				'getTranslations' => 'qwqer_translations_get',
				'updateTranslations' => 'qwqer_translations_post',

				'getWarehouses' => 'qwqer_warehouses_get',
				'updateWarehouse' => 'qwqer_warehouses_post',
				'deleteWarehouse' => 'qwqer_warehouses_delete',

				'getOrder' => 'qwqer_order_get',

				'getPrice' => 'qwqer_process_get_price',
				'placeOrder' => 'qwqer_process_place_order',

				'getShipmentByOrderId' => 'qwqer_shipment_by_order_id',
				'updateShipmentStatus' => 'qwqer_shipment_update_status',
				'listShipments' => 'qwqer_shipment_list',
				'rejectShipment' => 'qwqer_shipment_reject',

				'orderPage' => '/wp-admin/post.php?post=333&action=edit',
			]
		];

		return '<script>window.qwqerModule = ' . json_encode($qwqerModule) . ';</script>';
	}

	public function admin_menu()
	{
		echo $this->viewJsVariables();
		echo '<div class="wrap">
			<h1>QWQER Settings</h1>
			<div class="module--qwqer-delivery">
				<div class="q-tw-container q-tw-mx-auto">
				  <div id="module--qwqer-delivery--main">
					<div class="q-tw-text-center q-tw-bg-white q-tw-p-10 q-tw-rounded-md q-tw-mt-5 q-tw-border q-tw-border-gray-300">
					  Loading QWQER Components
					</div>
				  </div>
				</div>
			</div>
		</div>';
	}

	public function enqueue_order_details($orderId)
	{
        if (wc_get_order($orderId)->status === 'auto-draft') {
            return 'Create order first';
        }

		echo $this->viewJsVariables();

		echo '<script>
			window.qwqerModule.orderId = ' . $orderId . ';
			window.qwqerModule.carrierId = 0;
		</script>';

		echo '<div class="module--qwqer-delivery">
			<div id="module--qwqer-delivery--order"></div>
		</div>';
	}

	/**
	 * Register the stylesheets for the admin area.
	 */
	public function enqueue_styles()
	{
		wp_enqueue_style($this->qwqer_delivery, plugin_dir_url(__FILE__) . 'css/qwqer-delivery-admin.css', [], $this->version, 'all');
	}

	/**
	 * Register the JavaScript for the admin area.
	 */
	public function enqueue_scripts()
	{
		// wp_enqueue_script($this->qwqer_delivery, plugin_dir_url(__FILE__) . 'js/qwqer-delivery-admin.js', ['jquery'], $this->version, false);
		wp_enqueue_script($this->qwqer_delivery, plugin_dir_url(__FILE__) . 'js/app.min.js', null, $this->version, true);
	}
}
