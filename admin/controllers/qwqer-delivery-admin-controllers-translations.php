<?php

class Qwqer_Delivery_Admin_Controllers_Translations extends Qwqer_Delivery_Admin_Controller
{
    public function get()
    {
        $this->ajaxRender([
            'data' => get_option('qwqer_delivery_translations')
        ]);
    }

    public function post()
    {
        update_option('qwqer_delivery_translations', $_POST['locales']);

        $this->get();
    }
}
