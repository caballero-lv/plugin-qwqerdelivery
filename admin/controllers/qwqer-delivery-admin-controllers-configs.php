<?php

class Qwqer_Delivery_Admin_Controllers_Configs extends Qwqer_Delivery_Admin_Controller
{
	protected $requestService;
	protected $passwordService;

	public function __construct()
	{
		$this->requestService = new Qwqer_Delivery_Request();
		$this->passwordService = new Qwqer_Delivery_Password();
	}

	public function get()
	{
		$this->ajaxRender([
			'data' => [
				'enabled' => (bool)get_option('qwqer_delivery_enabled'),
				'login' => get_option('qwqer_delivery_login', null),
				'password' => get_option('qwqer_delivery_password') ? true : false,
			]
		]);
	}

	public function post()
	{
		$errorsBag = [];

		$postData = [
			'enabled' => (bool)($_POST['enabled'] ?: false),
			'login' => trim($_POST['login'] ?: null),
			'password' => trim($_POST['password'] ?: null),
		];

		$currentPassword = get_option('qwqer_delivery_password', null);

		if (!$postData['login']) {
			$errorsBag['login'] = 'Login is required';
		} else if (strlen($postData['login']) < 4) {
			$errorsBag['login'] = 'Login must have not least than 4 symbols';
		}

		if (!$currentPassword && !$postData['password']) {
			$errorsBag['password'] = 'Password is required';
		} else if ($postData['password'] && strlen($postData['password']) < 6) {
			$errorsBag['password'] = 'Password must have not least than 6 symbols';
		}

		if (!empty($errorsBag)) {
			$this->ajaxRender(['errors' => $errorsBag]);
		}

		if (get_option('qwqer_delivery_login', null) !== $postData['login'] || $postData['password']) {
			try {
				$this->requestService->post('/api/xr/mch/login', [
					'login' => $postData['login'],
					'passw' => isset($postData['password']) && $postData['password']
						? $postData['password']
						: $this->passwordService->decrypt($currentPassword)
				]);
			} catch (exception $exception) {
				$this->ajaxRender(['error' => $exception->getMessage()]);
			}
		}

		update_option('qwqer_delivery_enabled', $postData['enabled']);
		update_option('qwqer_delivery_login', $postData['login']);

		if ($postData['password']) {
			update_option('qwqer_delivery_password', $this->passwordService->encrypt($postData['password']));
		}

		$this->get();
	}
}
