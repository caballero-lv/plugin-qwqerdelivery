<?php

class Qwqer_Delivery_Admin_Controllers_Process extends Qwqer_Delivery_Admin_Controller
{
	protected $requestService;
	protected $passwordService;

	public function __construct()
	{
		$this->requestService = new Qwqer_Delivery_Request();
		$this->passwordService = new Qwqer_Delivery_Password();
	}

	public function get_price()
	{
		$validationErrors = [];

		$orderId = (int)($_POST['orderId'] ?: 0);
		$warehouseId = (int)($_POST['warehouseId'] ?: 0);
		$data = $_POST['data'] ?: [];
		$token = $_POST['authToken'] ?: null;

		if (empty($data['country'])) {
			$validationErrors['country'] = 'Country is required';
		}
		if (empty($data['countrycode2'])) {
			$validationErrors['countrycode2'] = 'Country ISO-2 is required';
		}
		if (empty($data['city'])) {
			$validationErrors['city'] = 'City is required';
		}
		if (empty($data['zipcode'])) {
			$validationErrors['zipcode'] = 'ZIP-code is required';
		}
		if (empty($data['address'])) {
			$validationErrors['address'] = 'Address is required';
		}

		if (!empty($validationErrors)) {
			$this->ajaxRender(['errors' => $validationErrors]);
		}

		$order = wc_get_order($orderId);
		if (!$order) {
			$this->ajaxRender(['error' => 'Order does not exist']);
		}

		$warehouse = Qwqer_Delivery_Model_Warehouse::getById($warehouseId);
		if (!$warehouse) {
			$this->ajaxRender(['error' => 'Warehouse does not exist']);
		}

		// $parcelsTotalWeight = 0;
		// foreach ($order->getProducts() as $product) {
		// 	$parcelsTotalWeight += isset($product['product_weight']) ? $product['product_weight'] : 0;
		// }

        $senderData = array_merge(
            [
                'name' => get_the_title(get_option('woocommerce_shop_page_id')),
                // 'phone' => Configuration::get('PS_SHOP_PHONE') ?: null,
                // 'contact' => Configuration::get('PS_SHOP_PHONE') ?: null,
                // 'email' => Configuration::get('PS_SHOP_EMAIL') ?: null,
                // 'company' => Configuration::get('PS_SHOP_NAME') ?: null
            ],
            $warehouse->data,
            [
                'address' => implode(', ', array_filter([
                    $warehouse->data['address'],
                    $warehouse->data['city'],
                    $warehouse->data['country'],
                    $warehouse->data['state'],
                    $warehouse->data['region'],
                    $warehouse->data['zipcode']
                ]))
            ]
        );

		$receiverData = array_merge([
            'name' => implode(' ', array_filter([
                $order->get_data()['shipping']['first_name'] ?: $order->get_data()['billing']['first_name'],
                $order->get_data()['shipping']['last_name'] ?: $order->get_data()['billing']['last_name']
            ])),
            'contact' => $order->get_data()['billing']['phone'],
            'phone' => $order->get_data()['billing']['phone'],
            'email' => $order->get_data()['billing']['email'],
            'company' => $order->get_data()['shipping']['company'] ?: $order->get_data()['billing']['company']
        ], $data);

		$ordersizeData = [
            'length'     => 0,
            'width'      => 0,
            'height'     => 0,
            // 'weight' => round($parcelsTotalWeight) ?: 1,
            'weight'     => 1,
            'lenunit'    => 'CENTIMETER',
            'weightunit' => 'KILOGRAM',
        ];

        $cacheHash = md5( implode( '|', $senderData ) )
                     . ':' . md5( implode( '|', $receiverData ) )
                     . ':' . md5( implode( '|', $ordersizeData ) );

        $cachedPrice = Qwqer_Delivery_Model_Calculated_Price::getByHash($cacheHash);

        if ($cachedPrice && time() - strtotime($cachedPrice->data['address_delivery_date']) <= 2592000) {
            $this->ajaxRender([
                'data' => [
                    'token' => null,
                    'response' => json_decode($cachedPrice->data['payload'], true),
                    'price' => $cachedPrice->data['price'] / 100,
                ]
            ]);
        }

		// Login to QWQER Api
		if (!$token) {
			try {
				$loginResponse = $this->requestService->post('/api/xr/mch/login', [
					'login' => get_option('qwqer_delivery_login'),
					'passw' => $this->passwordService->decrypt(get_option('qwqer_delivery_password'))
				]);

				$token = $loginResponse['data']['restid'];
			} catch (exception $exception) {
				$this->ajaxRender(['error' => $exception->getMessage()]);
			}
		}

		// Delivery order price from QWQER Api
		try {
			$deliveryOrderPriceResponse = $this->requestService->post('/api/xr/mch/delivery_price', [
				'sender' => $senderData,
				'receiver' => $receiverData,
				'ordersize' => $ordersizeData,
			], [
				"Authorization: Bearer {$token}"
			]);
		} catch (exception $exception) {
			$this->ajaxRender(['error' => $exception->getMessage()]);
		}

        if (!$cachedPrice) {
            $cachedPrice = new Qwqer_Delivery_Model_Calculated_Price(['hash' => $cacheHash]);
        }

        $cachedPrice->data['price'] = (int)((float)$deliveryOrderPriceResponse['data']['price'] * 100);
        $cachedPrice->data['payload'] = json_encode($deliveryOrderPriceResponse['data']);
        $cachedPrice->data['address_delivery_date'] = date('Y-m-d H:i:s');
        $cachedPrice->save();

		$this->ajaxRender([
			'data' => [
				'token' => $token,
				'response' => $deliveryOrderPriceResponse['data'],
				'price' => (float)$deliveryOrderPriceResponse['data']['price']
			]
		]);
	}

	public function place_order()
	{
		$order = wc_get_order((int)($_POST['orderId'] ?: 0));
		if (!$order) {
			$this->ajaxRender(['error' => 'Order does not exist']);
		}

		$calculatedData = $_POST['calculatedData'] ?: [];
		$token = $_POST['authToken'] ?: null;

		// Login to QWQER Api
		if (!$token) {
			try {
				$loginResponse = $this->requestService->post('/api/xr/mch/login', [
					'login' => get_option('qwqer_delivery_login'),
					'passw' => $this->passwordService->decrypt(get_option('qwqer_delivery_password'))
				]);

				$token = $loginResponse['data']['restid'];
			} catch (exception $exception) {
				$this->ajaxRender(['error' => $exception->getMessage()]);
			}
		}

		// Place order
		try {
			$deliveryCreateResponse = $this->requestService->post('/api/xr/mch/delivery', [
				'distance' => $calculatedData['distance'],
				'dunit' => 'METER',
				'dkind' => 'NONE',
				'country' => $calculatedData['sender']['countrycode2'],
				'duration' => $calculatedData['duration'],
				'summa' => $calculatedData['price'] - (isset($calculatedData['discount']) ? $calculatedData['discount'] : 0),
				'sender' => $calculatedData['sender'],
				'receiver' => $calculatedData['receiver'],
				'ordersize' => $calculatedData['ordersize'],
				'status' => 1,
			], [
				"Authorization: Bearer {$token}"
			]);
		} catch (exception $exception) {
			$this->ajaxRender(['error' => $exception->getMessage()]);
		}

		// Make payment
		try {
			$this->requestService->post('/api/xr/mch/delivery_payment', [
				'id' => $deliveryCreateResponse['data']['id']
			], [
				"Authorization: Bearer {$token}"
			]);
		} catch (exception $exception) {
			$this->ajaxRender(['error' => $exception->getMessage()]);
		}

		$shipment = new Qwqer_Delivery_Model_Shipment();
		$shipment->data['id_order'] = $order->id;
		$shipment->data['id_qwqer_order'] = $deliveryCreateResponse['data']['id'];
		$shipment->data['status'] = (int)$deliveryCreateResponse['data']['status'];
		$shipment->data['price'] = (int)($deliveryCreateResponse['data']['price'] * 100);
		$shipment->data['payload'] = json_encode($deliveryCreateResponse['data']);
		$shipment->save();

		$this->ajaxRender(['data' => true]);
	}
}
