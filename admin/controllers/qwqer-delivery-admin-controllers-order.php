<?php

class Qwqer_Delivery_Admin_Controllers_Order extends Qwqer_Delivery_Admin_Controller
{
	public function get()
	{
		$orderId = (int)($_POST['id'] ?: 0);
		$order = wc_get_order($orderId);

		if (!$order) {
			$this->ajaxRender(['error' => 'Order does not exist']);
		}

		// echo '<pre>';
		// var_dump($order);
		// echo '</pre>';
		// die();

		// $customer = new Customer($order->id_customer);
		// if (!$customer->id) {
		// 	$this->ajaxRenderJson(['error' => 'Order customer does not exist']);
		// }

		// $address = new Address($order->id_address_delivery);
		// if (!$address->id) {
		// 	$this->ajaxRenderJson(['error' => 'Order address does not exist']);
		// }

		// $country = new Country($address->id_country);
		// if (!$country->id) {
		// 	$this->ajaxRenderJson(['error' => 'Address country does not exist']);
		// }

		// $state = new State($address->id_state);
		// if (!$state->id) {
		// 	$state = null;
		// }

		$this->ajaxRender([
			// 'data' => compact('order', 'customer', 'address', 'country', 'state')
			'data' => $order->get_data()
		]);
	}
}
