<?php

class Qwqer_Delivery_Admin_Controllers_Warehouses extends Qwqer_Delivery_Admin_Controller
{
	public function get()
	{
		$this->ajaxRender(['data' => Qwqer_Delivery_Model_Warehouse::all()]);
	}

	public function post()
	{
		$errorsBag = [];

		$id = $_POST['id'] ?: null;
		$data = $_POST['data'] ?: [];

		if (empty($data['country'])) {
			$errorsBag['country'] = 'Country is required';
		}
		if (empty($data['countrycode2'])) {
			$errorsBag['countrycode2'] = 'Country ISO-2 is required';
		}
		if (empty($data['city'])) {
			$errorsBag['city'] = 'City is required';
		}
		if (empty($data['zipcode'])) {
			$errorsBag['zipcode'] = 'ZIP-code is required';
		}
		if (empty($data['address'])) {
			$errorsBag['address'] = 'Address is required';
		}

		if ($id && !$warehouse = Qwqer_Delivery_Model_Warehouse::getById($id)) {
			$errorsBag['model'] = 'Entry does not exist';
		}

		if (!empty($errorsBag)) {
			$this->ajaxRender(['errors' => $errorsBag]);
		}

		if (isset($data['is_default']) && $data['is_default']) {
			Qwqer_Delivery_Model_Warehouse::removeDefaultExlcludeId($id);
		}

		if (!isset($warehouse)) {
			$warehouse = new Qwqer_Delivery_Model_Warehouse();
		}

		$warehouse->data = $data;
		$warehouse->save();

		$this->ajaxRender(['data' => $warehouse->toArray()]);
	}

	public function delete()
	{
		$warehouse = Qwqer_Delivery_Model_Warehouse::getById($_POST['id'] ?: 0);

		if (!$warehouse) {
			$this->ajaxRender(['error' => 'Entry does not exist']);
		}

		$warehouse->delete();

		$this->ajaxRender(['data' => true]);
	}
}
