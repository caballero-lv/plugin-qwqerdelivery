<?php

class Qwqer_Delivery_Admin_Controllers_Shipment extends Qwqer_Delivery_Admin_Controller
{
	protected $requestService;
	protected $passwordService;

	public function __construct()
	{
		$this->requestService = new Qwqer_Delivery_Request();
		$this->passwordService = new Qwqer_Delivery_Password();
	}

	public function get_all()
	{
		$self = new Qwqer_Delivery_Model_Shipment();

		$filter = $_POST['filter'] ?: [];
		$page = (int)($_POST['page'] ?: 1);

		$shipments = Qwqer_Delivery_Model_Shipment::getAll(isset($filter['status']) ? $filter['status'] : null, $page);

		$perPage = $self->getPerPage();
		$total = $self->getPagesCount();

		foreach ($shipments as &$shipment) {
			$shipment['order'] = wc_get_order($shipment['id_order'])->get_data();
		}

		$this->ajaxRender([
			'data' => $shipments,
			'meta' => [
				'current_page' => $page,
				'from' => (($page - 1) * $perPage) + 1,
				'last_page' => ceil($total / $perPage),
				'per_page' => $perPage,
				'to' => (($page - 1) * $perPage) + $perPage,
				'total' => $total
			]
		]);
	}

	public function get_by_order_id()
	{
		$id = (int)($_POST['id'] ?: 0);
		$shipment = Qwqer_Delivery_Model_Shipment::getByOrderId($id);

		$this->ajaxRender(['data' => $shipment ? $shipment->toArray() : null]);
	}

	public function update_status()
	{
		$id = (int)($_POST['id'] ?: 0);

		$shipment = Qwqer_Delivery_Model_Shipment::getById($id);
		$token = null;

		if (!$shipment) {
			$this->ajaxRender(['error' => 'Shipment does not exist']);
		}

		// Login to QWQER Api
		try {
			$loginResponse = $this->requestService->post('/api/xr/mch/login', [
				'login' => get_option('qwqer_delivery_login'),
				'passw' => $this->passwordService->decrypt(get_option('qwqer_delivery_password'))
			]);

			$token = $loginResponse['data']['restid'];
		} catch (exception $exception) {
			$this->ajaxRender(['error' => $exception->getMessage()]);
		}

		// Fetch order from QWQER API
		try {
			$orderDataResponse = $this->requestService->get('/api/xr/mch/delivery/' . $shipment->data['id_qwqer_order'], [], [
				"Authorization: Bearer {$token}"
			]);
		} catch (exception $exception) {
			$this->ajaxRender(['error' => $exception->getMessage()]);
		}

		$shipment->data['status'] = $orderDataResponse['data']['status'];
		$shipment->save();

		$this->ajaxRender(['data' => $shipment->toArray()]);
	}
}
