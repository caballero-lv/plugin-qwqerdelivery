<?php

abstract class Qwqer_Delivery_Admin_Controller
{
	protected $pluginName = 'qwqer_delivery';

	protected function ajaxRender($content)
	{
		header('Content-Type: application/json; charset=utf-8');

		echo json_encode($content, JSON_NUMERIC_CHECK);

		wp_die();
	}
}
