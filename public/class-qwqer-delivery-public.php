<?php

class Qwqer_Delivery_Public
{
    /** @var string */
	private $qwqer_delivery;

    /** @var string */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @param string $qwqer_delivery The name of the plugin.
	 * @param string $version The version of this plugin.
	 */
	public function __construct($qwqer_delivery, $version)
	{
		$this->qwqer_delivery = $qwqer_delivery;
		$this->version = $version;
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 */
	public function enqueue_styles()
	{
		wp_enqueue_style($this->qwqer_delivery, plugin_dir_url(__FILE__) . 'css/qwqer-delivery-public.css', [], $this->version, 'all');
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 */
	public function enqueue_scripts()
	{
		wp_enqueue_script($this->qwqer_delivery, plugin_dir_url(__FILE__) . 'js/qwqer-delivery-public.js', ['jquery'], $this->version, false);
	}
}
